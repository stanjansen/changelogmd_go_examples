package main_test

import (
	"testing"
)

type Point struct {
	X, Y int
}

func BenchmarkPointers(b *testing.B) {
	b.Run("no_pointer", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			points := make([]Point, 10000)
			for j := 0; j < 10000; j++ {
				points[j] = Point{X: j, Y: j}
			}
			for j := 0; j < 1000; j++ {
				points2 := make([]Point, 10000)
				for k := 0; k < 10000; k++ {
					points2[k] = points[k]
				}
			}
		}
	})

	b.Run("pointer", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			points := make([]*Point, 10000)
			for j := 0; j < 10000; j++ {
				points[j] = &Point{X: j, Y: j}
			}
			for j := 0; j < 1000; j++ {
				points2 := make([]*Point, 10000)
				points2 = append(points2, points...)
			}
		}
	})
}
