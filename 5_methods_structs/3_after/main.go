package main

import (
	"fmt"
)

type Object struct {
	Name     string
	Velocity int
	*Point
}

type Point struct {
	X, Y int
}

type ObjectCollection struct {
	objects []*Object
	above   map[Object][]Object
}

func main() {
	oc := createObjectCollection()

	for {
		if ok := oc.MoveAll(); !ok {
			// No object moved, all landed
			break
		}

		oc.Check()
	}
}

func createObjectCollection() ObjectCollection {
	return ObjectCollection{
		objects: []*Object{
			{Name: "feather", Velocity: 1, Point: &Point{X: 1, Y: 100}},
			{Name: "piano", Velocity: 5, Point: &Point{X: 1, Y: 400}},
			{Name: "rock", Velocity: 2, Point: &Point{X: 1, Y: 1000}},
			{Name: "potato", Velocity: 2, Point: &Point{X: 2, Y: 150}},
			{Name: "bag of potatos", Velocity: 5, Point: &Point{X: 2, Y: 300}},
		},
	}
}

func (oc *ObjectCollection) MoveAll() (objectMoved bool) {
	oc.mapAbove()
	for _, o := range oc.objects {
		if ok := o.Move(); ok {
			objectMoved = true
		}
	}
	return
}

func (o *Object) Move() bool {
	if o.Point.Y != 0 {
		o.Point.Y -= o.Velocity
		if o.Point.Y < 0 {
			o.Point.Y = 0
		}
		return true
	}

	return false
}

func (oc *ObjectCollection) mapAbove() {
	oc.above = make(map[Object][]Object, len(oc.objects))
	for _, a := range oc.objects {
		oc.above[*a] = []Object{}

		for _, b := range oc.objects {
			if a == b {
				continue
			}

			if a.X == b.X && a.Y < b.Y {
				oc.above[*a] = append(oc.above[*a], *b)
			}
		}
	}
}

func (oc *ObjectCollection) Check() {
	for _, a := range oc.objects {
		for _, b := range oc.above[*a] {
			if a.Point.Y > b.Point.Y {
				fmt.Printf("%s (%d) passed %s (%d)\n", a.Name, a.Point.Y, b.Name, b.Point.Y)
			} else if a.Point.Y == b.Point.Y {
				fmt.Printf("%s (%d) meets %s (%d)\n", a.Name, a.Point.Y, b.Name, b.Point.Y)
			}
		}
	}
}
