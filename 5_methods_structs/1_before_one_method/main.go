package main

import (
	"fmt"
)

type Object struct {
	Name     string
	Velocity int
	*Point
}

type Point struct {
	X, Y int
}

func main() {
	objects := []*Object{
		{Name: "feather", Velocity: 1, Point: &Point{X: 1, Y: 100}},
		{Name: "piano", Velocity: 5, Point: &Point{X: 1, Y: 400}},
		{Name: "rock", Velocity: 2, Point: &Point{X: 1, Y: 1000}},
		{Name: "potato", Velocity: 2, Point: &Point{X: 2, Y: 150}},
		{Name: "bag of potatos", Velocity: 5, Point: &Point{X: 2, Y: 300}},
	}

	for {
		// Map all objects above each other
		objectsAbove := make(map[Object][]Object, len(objects))
		for _, a := range objects {
			objectsAbove[*a] = []Object{}
			for _, b := range objects {
				if a == b {
					continue
				}

				if a.X == b.X && a.Y < b.Y {
					objectsAbove[*a] = append(objectsAbove[*a], *b)
				}
			}
		}

		// Move all objects
		objectMoved := false
		for _, o := range objects {
			if o.Point.Y != 0 {
				objectMoved = true
				o.Point.Y -= o.Velocity
				if o.Point.Y < 0 {
					o.Point.Y = 0
				}
			}
		}
		if !objectMoved {
			// No object moved, all landed
			break
		}

		// Check if objects passed each other
		for _, a := range objects {
			for _, b := range objectsAbove[*a] {
				if a.Point.Y > b.Point.Y {
					fmt.Printf("%s (%d) passed %s (%d)\n", a.Name, a.Point.Y, b.Name, b.Point.Y)
				} else if a.Point.Y == b.Point.Y {
					fmt.Printf("%s (%d) meets %s (%d)\n", a.Name, a.Point.Y, b.Name, b.Point.Y)
				}
			}
		}
	}
}
