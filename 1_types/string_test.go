package main_test

import (
	"strings"
	"testing"
)

func BenchmarkStringTypes(b *testing.B) {
	input := "xxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxooooxxxxoooxoxoxxxooooxxxoooooooooxxxxxxxxxxxooooooxoxoxoxxxxxxxxxxxxooooooooooooooooxxxxxxxxxxxxxxxxooooooooooooooooooooooxxxoooo"

	b.Run("string", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			chars := strings.Split(input, "")
			var x, o int
			for _, c := range chars {
				if c == "x" {
					x++
				} else if c == "o" {
					o++
				}
			}
		}
	})

	b.Run("byte", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			chars := []byte(input)
			var x, o int
			for _, c := range chars {
				if c == 'x' {
					x++
				} else if c == 'o' {
					o++
				}
			}
		}
	})

	b.Run("rune", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			chars := []rune(input)
			var x, o int
			for _, c := range chars {
				if c == 'x' {
					x++
				} else if c == 'o' {
					o++
				}
			}
		}
	})
}
