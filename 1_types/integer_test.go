package main_test

import (
	"testing"
)

func BenchmarkIntegerTypes(b *testing.B) {
	b.Run("int", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := []int{}
			var total int
			for j := 0; j < 255; j++ {
				for k := 0; k < 100000; k++ {
					digits = append(digits, j)
					total += j
				}
			}
			total = total / len(digits)
		}
	})

	b.Run("int64", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := []int64{}
			var total int64
			for j := int64(0); j < 255; j++ {
				for k := 0; k < 100000; k++ {
					digits = append(digits, j)
					total += j
				}
			}
			total = total / int64(len(digits))
		}
	})

	b.Run("uint16", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := []uint16{}
			var total uint16
			for j := uint16(0); j < 255; j++ {
				for k := 0; k < 100000; k++ {
					digits = append(digits, j)
					total += j
				}
			}
			total = total / uint16(len(digits))
		}
	})

	b.Run("byte", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := []byte{}
			var total byte
			for j := byte(0); j < 255; j++ {
				for k := 0; k < 100000; k++ {
					digits = append(digits, j)
					total += j
				}
			}
			total = total / byte(len(digits))
		}
	})
}
