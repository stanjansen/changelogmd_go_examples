package main

import (
	"bytes"
	"fmt"
)

type Board struct {
	Fields [][]byte
	X, Y   byte
}

func main() {
	instructions := "WWDDAAAADDAWWSASADADADWDWAASWAAWWWAWASSDWWDADWWDADAWADWSSADSDWWSAASDWWWWAASDWSSWSADWSDSAAAADASSAAAAWDASSSAWSWWDWDASADSSASAWDWWWWDWWWASASASADWDDSDDSAAADASSAWDWWDWAADAWASWADDDWSAAWSSWWASSDAWAWWADSDSAADDAWWSDDSSDAASSWWWAAASWASAWSSWDWSAWDSWSWSADWSWSAWADWWSWSWSDWSWWSWSDSADAASWSDWSADSWSWSSSAWWSSDADDAWSWWWSSSSADWAAWSSSAAAAADDWWWSDASDDWSWSDAWADDSWDDAWDSWWSASWAADAWSSDSDSASWWWADDWSSWWSASSASAWWAAASADADSSSWASWAWADDDSWWWAWAASWWADSAWDASDDWSDWDDDDDWADWAAAWAADWDWASWSSSDAWADSADSWAWASADADDADSAAADWAWSWDSDWWSSSAAWAWADWSDSDSADAWDWDAWWDDSDWWSSDWSSWDDAAASWDWSDAAWSWWDSAASSDWWWDADSWWADSDWASAAWASWWAAASWDSSWAWAADSSDADSWSWWDSADWDWSWAWWSWWSWWSSDDSDDSSADSDWASADDDWSWDWAWASAWWDDSDSADDSSAWDDAWDWASWWAWSSDSWSDADWAAADASWDWDDDDDWAWWWSWDDDSDDASSAWWWWWASSSWSDWSASSDSADSASSAAWWASDDDDSDWSDWWDDDWDDAWAAWSSWSASASAASDWSWDAADWDWWDWDDADASSSSWSAWWSSSDWSAAWSDASSDDSWWDSDAASAAWDSDADSWSDAWSDDDWADDSWWDSDDWDWDASAAWAAWWSWDAADASSDDDDWSWAWWDSASWSDAAWAWSSAWSSSAAADASDAASSSSWASWWADDWWSDDWSWWDDAWASWWSAWAAADAAWDWWDWWSDDAAAWSAAWDDSASWDSSWADAWWSSSAS"
	input := `..<...X..v...
	......X..>.<.
	..X.X....^...
	.>..X...X....
	......X......
	...<....^.X..
	..X...X...X..`
	board := createBoard(input)

	for _, c := range instructions {
		board.Move(c)
	}

	fmt.Printf("Final position: %d, %d\n", board.X, board.Y)
}

func createBoard(input string) Board {
	return Board{
		X:      0,
		Y:      0,
		Fields: bytes.Fields([]byte(input)),
	}
}

func (b *Board) Move(dir rune) {
	switch dir {
	case 'W':
		b.Y++
	case 'S':
		b.Y--
	case 'A':
		b.X--
	case 'D':
		b.X++
	}

	if b.X < 0 {
		b.X = 0
	} else if b.X >= byte(len(b.Fields[0])) {
		b.X = byte(len(b.Fields[0])) - 1
	}

	if b.Y < 0 {
		b.Y = 0
	} else if b.Y >= byte(len(b.Fields)) {
		b.Y = byte(len(b.Fields)) - 1
	}

	switch b.Current() {
	case 'X':
		switch dir {
		case 'W':
			b.Move('S')
		case 'S':
			b.Move('W')
		case 'A':
			b.Move('D')
		case 'D':
			b.Move('A')
		}
	case '<':
		b.Move('A')
	case '>':
		b.Move('D')
	case '^':
		b.Move('W')
	case 'v':
		b.Move('S')
	}
}

func (b *Board) Current() rune {
	return rune(b.Fields[b.Y][b.X])
}
