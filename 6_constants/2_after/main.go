package main

import (
	"bytes"
	"fmt"
)

type Board struct {
	Fields [][]byte
	X, Y   byte
}

type Direction rune
type Tile rune

const (
	Up    Direction = 'W'
	Down  Direction = 'S'
	Left  Direction = 'A'
	Right Direction = 'D'

	Obstacle  Tile = 'X'
	LeftTurn  Tile = '<'
	RightTurn Tile = '>'
	UpTurn    Tile = '^'
	DownTurn  Tile = 'v'
)

func main() {
	instructions := "WWDDAAAADDAWWSASADADADWDWAASWAAWWWAWASSDWWDADWWDADAWADWSSADSDWWSAASDWWWWAASDWSSWSADWSDSAAAADASSAAAAWDASSSAWSWWDWDASADSSASAWDWWWWDWWWASASASADWDDSDDSAAADASSAWDWWDWAADAWASWADDDWSAAWSSWWASSDAWAWWADSDSAADDAWWSDDSSDAASSWWWAAASWASAWSSWDWSAWDSWSWSADWSWSAWADWWSWSWSDWSWWSWSDSADAASWSDWSADSWSWSSSAWWSSDADDAWSWWWSSSSADWAAWSSSAAAAADDWWWSDASDDWSWSDAWADDSWDDAWDSWWSASWAADAWSSDSDSASWWWADDWSSWWSASSASAWWAAASADADSSSWASWAWADDDSWWWAWAASWWADSAWDASDDWSDWDDDDDWADWAAAWAADWDWASWSSSDAWADSADSWAWASADADDADSAAADWAWSWDSDWWSSSAAWAWADWSDSDSADAWDWDAWWDDSDWWSSDWSSWDDAAASWDWSDAAWSWWDSAASSDWWWDADSWWADSDWASAAWASWWAAASWDSSWAWAADSSDADSWSWWDSADWDWSWAWWSWWSWWSSDDSDDSSADSDWASADDDWSWDWAWASAWWDDSDSADDSSAWDDAWDWASWWAWSSDSWSDADWAAADASWDWDDDDDWAWWWSWDDDSDDASSAWWWWWASSSWSDWSASSDSADSASSAAWWASDDDDSDWSDWWDDDWDDAWAAWSSWSASASAASDWSWDAADWDWWDWDDADASSSSWSAWWSSSDWSAAWSDASSDDSWWDSDAASAAWDSDADSWSDAWSDDDWADDSWWDSDDWDWDASAAWAAWWSWDAADASSDDDDWSWAWWDSASWSDAAWAWSSAWSSSAAADASDAASSSSWASWWADDWWSDDWSWWDDAWASWWSAWAAADAAWDWWDWWSDDAAAWSAAWDDSASWDSSWADAWWSSSAS"
	input := `..<...X..v...
	......X..>.<.
	..X.X....^...
	.>..X...X....
	......X......
	...<....^.X..
	..X...X...X..`
	board := createBoard(input)

	for _, c := range instructions {
		board.Move(Direction(c))
	}

	fmt.Printf("Final position: %d, %d\n", board.X, board.Y)
}

func createBoard(input string) Board {
	return Board{
		X:      0,
		Y:      0,
		Fields: bytes.Fields([]byte(input)),
	}
}

func (b *Board) Move(dir Direction) {
	switch dir {
	case Up:
		b.Y++
	case Down:
		b.Y--
	case Left:
		b.X--
	case Right:
		b.X++
	}

	if b.X < 0 {
		b.X = 0
	} else if b.X >= byte(len(b.Fields[0])) {
		b.X = byte(len(b.Fields[0])) - 1
	}

	if b.Y < 0 {
		b.Y = 0
	} else if b.Y >= byte(len(b.Fields)) {
		b.Y = byte(len(b.Fields)) - 1
	}

	switch b.Current() {
	case Obstacle:
		switch dir {
		case Up:
			b.Move(Down)
		case Down:
			b.Move(Up)
		case Left:
			b.Move(Right)
		case Right:
			b.Move(Left)
		}
	case LeftTurn:
		b.Move(Left)
	case RightTurn:
		b.Move(Right)
	case UpTurn:
		b.Move(Up)
	case DownTurn:
		b.Move(Down)
	}
}

func (b *Board) Current() Tile {
	return Tile(b.Fields[b.Y][b.X])
}
