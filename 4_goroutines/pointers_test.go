package main_test

import (
	"sync"
	"testing"
)

type Point struct {
	X, Y int
}

func BenchmarkGoroutines(b *testing.B) {
	b.Run("no_goroutines", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			doStuff := func() {
				for j := 0; j < 1000; j++ {
					_ = j
				}
			}
			for j := 0; j < 1000; j++ {
				doStuff()
			}
		}
	})

	b.Run("goroutines", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			wg := sync.WaitGroup{}
			doStuff := func() {
				for j := 0; j < 1000; j++ {
					_ = j
					wg.Done()
				}
			}
			wg.Add(1000 * 1000)
			for j := 0; j < 1000; j++ {
				go doStuff()
			}
			wg.Wait()
		}
	})

	b.Run("no_goroutines_calc", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			var total int
			doStuff := func() {
				for j := 0; j < 1000; j++ {
					total += j
				}
			}
			for j := 0; j < 1000; j++ {
				doStuff()
			}
		}
	})

	b.Run("goroutines_calc", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			var total int
			wg := sync.WaitGroup{}
			mu := sync.Mutex{}
			doStuff := func() {
				for j := 0; j < 1000; j++ {
					mu.Lock()
					total += j
					wg.Done()
					mu.Unlock()
				}
			}
			wg.Add(1000 * 1000)
			for j := 0; j < 1000; j++ {
				go doStuff()
			}
			wg.Wait()
		}
	})
}
