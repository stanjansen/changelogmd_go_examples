package main_test

import (
	"testing"
)

func BenchmarkDoubleNestedSlicesMaps(b *testing.B) {
	b.Run("slice", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make([][]bool, 1000)
			for j := 0; j < 1000; j++ {
				digits[j] = make([]bool, 1000)
				for k := 0; k < 1000; k++ {
					digits[j][k] = true
				}
			}
		}
	})

	b.Run("map_nested", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make(map[int]map[int]bool, 1000)
			for j := 0; j < 1000; j++ {
				digits[j] = make(map[int]bool, 1000)
				for k := 0; k < 1000; k++ {
					digits[j][k] = true
				}
			}
		}
	})

	b.Run("map_composite", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make(map[int]bool, 1000*1000)
			for j := 0; j < 1000; j++ {
				for k := 0; k < 1000; k++ {
					digits[j*1000+k] = true
				}
			}
		}
	})
}

func BenchmarkTripleNestedSlicesMaps(b *testing.B) {
	b.Run("slice", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make([][][]bool, 100)
			for j := 0; j < 100; j++ {
				digits[j] = make([][]bool, 100)
				for k := 0; k < 100; k++ {
					digits[j][k] = make([]bool, 100)
					for l := 0; l < 100; l++ {
						digits[j][k][l] = true
					}
				}
			}
		}
	})

	b.Run("map_nested", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make(map[int]map[int]map[int]bool, 100)
			for j := 0; j < 100; j++ {
				digits[j] = make(map[int]map[int]bool, 100)
				for k := 0; k < 100; k++ {
					digits[j][k] = make(map[int]bool, 100)
					for l := 0; l < 100; l++ {
						digits[j][k][l] = true
					}
				}
			}
		}
	})

	b.Run("map_composite", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make(map[int]bool, 100*100*100)
			for j := 0; j < 100; j++ {
				for k := 0; k < 100; k++ {
					for l := 0; l < 100; l++ {
						digits[j*100*100+k*100+l] = true
					}
				}
			}
		}
	})
}
