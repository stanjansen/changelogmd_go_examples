package main_test

import (
	"testing"
)

func BenchmarkSlicesMaps(b *testing.B) {
	b.Run("slice_without_size", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := []bool{}
			for j := 0; j < 1000000; j++ {
				digits = append(digits, true)
			}
		}
	})

	b.Run("slice_with_size", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := [1000000]bool{}
			for j := 0; j < 1000000; j++ {
				digits[j] = true
			}
		}
	})

	b.Run("slice_with_capacity", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make([]bool, 0, 1000000)
			for j := 0; j < 1000000; j++ {
				digits = append(digits, true)
			}
		}
	})

	b.Run("slice_with_incorrect_capacity", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make([]bool, 0, 500000)
			for j := 0; j < 1000000; j++ {
				digits = append(digits, true)
			}
		}
	})

	b.Run("map_without_size", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := map[int]bool{}
			for j := 0; j < 1000000; j++ {
				digits[j] = true
			}
		}
	})

	b.Run("map_with_size", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			digits := make(map[int]bool, 1000000)
			for j := 0; j < 1000000; j++ {
				digits[j] = true
			}
		}
	})
}
